<?php



Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

                //ARTICLES
            Route::group(array("prefix" => "articles" ,"as"=>"articles."), function () {
                Route::get('{id}/copy', array('as'=>'copy', 'uses' => 'ArticleController@copy'));
                Route::get('articlerelation/{article_id?}', array('as'=>'articlerelation', 'uses' => 'ArticleController@getArticleRelation'));

                //CATEGORIES
                Route::group(array("prefix" => "categories", "as"=>"categories."), function () {
                    Route::get('{id}/copy', array('as'=>'{id}.copy', 'uses' => 'CategoryController@copy'));
                    Route::any('api/table', array('as'=>'api.table', 'uses' => 'CategoryController@getDatatable'));
                });
                Route::resource('categories', 'CategoryController');

                //API
                Route::group(array("prefix" => "api", "as"=>"api."), function () {
                    Route::any('table', array('as'=>'table', 'uses' => 'ArticleController@getDatatable'));
                });
            });
            Route::resource('articles', 'ArticleController');
        });
    });
});
