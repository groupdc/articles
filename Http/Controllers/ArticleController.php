<?php

namespace Dcms\Articles\Http\Controllers;

use Dcms\Articles\Models\Article;
use Dcms\Articles\Models\ArticleToArticle;
use Dcms\Articles\Models\Detail;
use Dcms\Articles\Models\Category;

//use Dcweb\Dcms\Models\Pages\Page;
//use Dcweb\Dcms\Controllers\Pages\PageController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use Form;
use DateTime;

class ArticleController extends Controller
{
    public $article_language_Columnnames = [];
    public $article_Columnnames = [];

    public $articlelanguageFormTemplate = "";
    public $articleFormTemplate = "";

    public $extendgeneralTemplate = "";

    public function __construct()
    {
        $this->middleware('permission:articles-browse')->only('index');
        $this->middleware('permission:articles-add')->only(['create', 'store']);
        $this->middleware('permission:articles-edit')->only(['edit', 'update']);
        $this->middleware('permission:articles-delete')->only('destroy');

        //note startdate and enddate are defaults, since these are always there, and have a specific value (not simple text, or bool)
        $this->article_Columnnames = [
            'thumbnail'     => 'thumbnail',
            'enabletime'    => 'enabletime',
            'birthdaycycle' => 'birthdaycycle',
            'highpriority'  => 'highpriority',
            'online'        => 'online',
            'newarticle'    => 'newarticle',
            'calendar'      => 'calendar',
            'archive'       => 'archive',
        ];

        $this->article_language_Columnnames = [
            'article_category_id' => 'category_id',
            'title'               => 'title',
            'description'         => 'description',
            'body'                => 'body',
            'slug'                => 'title',
            'path'                => 'title',
            'url'                 => 'url',
        ];

        $this->articlelanguageFormTemplate = null;
        $this->articleFormTemplate = null;
    }

    public static function getArticlesDetailByLanguage($language_id = null)
    {
        return Detail::where("language_id", "=", $language_id)->lists("title", "id");
    }

    public static function getDropdownArticlesByLanguage($language_id = null)
    {
        $dropdownvalues = ArticleController::getArticlesDetailByLanguage($language_id);

        return Form::select("article[" . $language_id . "][]", $dropdownvalues, [3, 4, 5, 6], ["id" => "articles-" . $language_id, "multiple" => "multiple"]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view
        return View::make('dcms::articles/index');
    }


    public function getDatatable()
    {
        return DataTables::queryBuilder(
            DB::connection('project')
                ->table('articles')
                ->select(
                    'articles.id',
                    'articles_language.title',
                    'articles_language.id as article_language_id',
                    'articles_categories_language.title as category',
                    (DB::connection("project")->raw('Concat("<img src=\'/packages/dcms/core/images/flag-",lcase(country),".png\' >") as country'))
                )
                ->join('articles_language', 'articles.id', '=', 'articles_language.article_id')
                ->leftJoin('articles_categories_language', 'articles_categories_language.id', '=', 'articles_language.article_category_id')
                ->leftJoin('languages', 'articles_language.language_id', '=', 'languages.id')
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/articles/' . $model->article_language_id . '" accept-charset="UTF-8" class="pull-right">
								<input name="_token" type="hidden" value="' . csrf_token() . '">
								<input name="_method" type="hidden" value="DELETE">';
                if (Auth::user()->can('articles-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/articles/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>';
                }
                if (Auth::user()->can('articles-add')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/articles/' . $model->article_language_id . '/copy"><i class="far fa-copy"></i></a>';
                }
                if (Auth::user()->can('articles-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';

                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as title, NULL as sort_id, (select max(sort_id) from articles_language where language_id = languages.id) as maxsort, '' as description , '' as body, '' as url, '' as article_category_id, '' as id")), "id as language_id", "language", "country", "language_name")->get();
        } else {
            return DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, article_category_id, articles_language.id, article_id, title, sort_id, (select max(sort_id) from articles_language as X  where X.language_id = articles_language.language_id) as maxsort,  description, body, url, date_format(startdate,\'%d-%m-%Y\') as startdate , date_format(enddate,\'%d-%m-%Y\')  as enddate
													FROM articles_language
													LEFT JOIN languages on languages.id = articles_language.language_id
													LEFT JOIN articles on articles.id = articles_language.article_id
													WHERE  languages.id is not null AND  article_id = ?
													UNION
													SELECT languages.id , language, country, language_name, \'\' , \'\' ,  \'\' , \'\' , NULL as sort_id, (select max(sort_id) from articles_language where language_id = languages.id) as maxsort, \'\' , \'\'  , \'\' , \'\' , \'\'
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM articles_language WHERE article_id = ?) ORDER BY 1
													', [$id, $id]);
        }
    }

    public function getExtendedModel()
    {
        //do nothing let the extend class hook into this
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = $this->getInformation();

        // load the create form (app/views/articles/create.blade.php)
        return View::make('dcms::articles/form')
            ->with('languages', $languages)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('pageOptionValuesSelected', [])
            ->with('sortOptionValues', $this->getSortOptions($languages, 1))
            ->with('articlelanguageFormTemplate', $this->articlelanguageFormTemplate)
            ->with('articleFormTemplate', $this->articleFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        $cat = $article->category;

        // show the view and pass the nerd to it
        return View::make('dcms::articles/show')
            ->with('article', $article)
            ->with("category", $cat->title);
    }

    public function getSortOptions($model, $setExtra = 0)
    {
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->id) <= 0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i <= ($maxSortID + $increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }

        return $SortOptions;
    }

    public function getSelectedPages($articleid = null)
    {
        return DB::connection("project")->select('  SELECT article_detail_id, page_id
													FROM articles_language_to_pages
													WHERE article_detail_id IN (SELECT id FROM articles_language WHERE article_id = ?)', [$articleid]);
    }

    public function setPageOptionValues($objselected_pages)
    {
        $pageOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $pageOptionValuesSelected[$obj->page_id] = $obj->page_id;
            }
        }

        return $pageOptionValuesSelected;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $article->startdate = (is_null($article->startdate) ? null : DateTime::createFromFormat('Y-m-d', $article->startdate)->format('m/d/Y'));
        $article->enddate = (is_null($article->enddate) ? null : DateTime::createFromFormat('Y-m-d', $article->enddate)->format('m/d/Y'));

        $objlanguages = $this->getInformation($id);
        $objselected_pages = $this->getSelectedPages($id);

        return View::make('dcms::articles/form')
            ->with('article', $article)
            ->with('languages', $objlanguages)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('pageOptionValuesSelected', $this->setPageOptionValues($objselected_pages))
            ->with('sortOptionValues', $this->getSortOptions($objlanguages))
            ->with('articlelanguageFormTemplate', $this->articlelanguageFormTemplate)
            ->with('articleFormTemplate', $this->articleFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * copy the model
     *
     * @param  int $id
     *
     * @return Response
     */
    public function copy($id)
    {
        $newDetail = Detail::find($id)->replicate();
        $Article = new Article();
        $Article->save();
        $newDetail->article_id = $Article->id;
        $newDetail->save();

        return Redirect::to('admin/articles');
    }


    private function validateArticleForm()
    {
        $rules = [
            //	'title' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }


    private function saveArticleProperties(Request $request, $articleid = null)
    {
        // do check if the given id is existing.
        if (!is_null($articleid) && intval($articleid) > 0) {
            $Article = Article::find($articleid);
        }

        if (!isset($Article) || is_null($Article)) {
            $Article = new Article;
        }

        foreach ($request->get("title") as $language_id => $title) {
            if (strlen(trim($request->get("title")[$language_id])) > 0 || strlen(trim($request->get("description")[$language_id])) > 0 || strlen(trim($request->get("body")[$language_id])) > 0) {
                //----------------------------------------
                // once there is a title set
                // we can set the properties to the article
                // and instantly return the article model
                // by default nothing is return so the
                // script may stop.
                //----------------------------------------
                $Article->startdate = (!empty($request->get("startdate")) ? DateTime::createFromFormat('m/d/Y', $request->get("startdate"))->format('Y-m-d') : null);
                $Article->enddate = (!empty($request->get("enddate")) ? DateTime::createFromFormat('m/d/Y', $request->get("enddate"))->format('Y-m-d') : null);

                foreach ($this->article_Columnnames as $column => $inputname) {
                    if ($request->has($inputname)) {
                        $Article->$column = $request->get($inputname);
                    } else {
                        $Article->$column = null;
                    }
                }

                //$Article->admin =  Auth::dcms()->user()->username;
                $Article->save();

                return $Article;
                break; // we only have to save the global settings once.
            }
        }
        //does not return anything by defuault... (may be false dunno - find out)
    }

    private function saveArticleDetail(Request $request, Article $Article, $givenlanguage_id = null)
    {
        $input = $request->all();

        $Detail = null;

        foreach ($input["title"] as $language_id => $title) {
            if (strlen(trim($input["title"][$language_id])) > 0 || strlen(trim($input["description"][$language_id])) > 0 || strlen(trim($input["body"][$language_id])) > 0) {
                if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id))) {
                    $Detail = null;
                    $newInformation = true;
                    $Detail = Detail::find($input["article_information_id"][$language_id]);

                    if (is_null($Detail) === true) {
                        $Detail = new Detail();
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($Detail->sort_id) && intval($Detail->sort_id) > 0) {
                        $oldSortID = intval($Detail->sort_id);
                    }

                    $Detail->language_id = $language_id;

                    foreach ($this->article_language_Columnnames as $column => $inputname) {
                        if (!in_array($column, ["slug", "path"])) {
                            $Detail->$column = $input[$inputname][$language_id];
                        }
                    }
                    $Detail->article_category_id = ($input[$this->article_language_Columnnames["article_category_id"]][$language_id] == 0 ? null : $input[$this->article_language_Columnnames["article_category_id"]][$language_id]);
                    $Detail->url_slug = Str::slug($input[$this->article_language_Columnnames["slug"]][$language_id]);
                    $Detail->url_path = Str::slug($input[$this->article_language_Columnnames["path"]][$language_id]);

                    $Detail->save();
                    $Article->detail()->save($Detail);

                    //----------------------------------------
                    // Detach the $Detail from all pages
                    //----------------------------------------
                    $Detail->pages()->detach();

                    //----------------------------------------
                    // link the article to the selected page
                    // we will take the $Detail->id since
                    // this directly holds the language_id
                    // otherwise we'd be storing it twice
                    //----------------------------------------
                    if (isset($input["page_id"]) && isset($input["page_id"][$language_id]) && count($input["page_id"][$language_id]) > 0) {
                        foreach ($input["page_id"][$language_id] as $arrayindex => $page_id) {
                            $Detail->pages()->attach($page_id);
                        }
                    }
                }
            }
        }

        return $Detail;
    }

    public function getSelectedProductsInformation($article_id = null)
    {
        return DB::connection("project")->select('	SELECT products_information_id, articles_language_id
													FROM articles_language_to_products_information
													WHERE articles_language_id IN (SELECT id FROM articles_language WHERE article_id = ?)', [$article_id]);
    }

    public function setProductsInformationOptionValues($objselected_pages)
    {
        $ProductsOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $ProductsOptionValuesSelected[$obj->products_information_id] = $obj->products_information_id;
            }
        }

        return $ProductsOptionValuesSelected;
    }

    public function saveArticleToProductInformation(Article $Article)
    {/*
        $input = $request->get();

        $Article->productinformation()->detach();
        if(isset($input["information_group_id"]) && count($input["information_group_id"])>0)
        {
            foreach($input["information_group_id"] as $i => $information_group_id) $Article->productinformation()->attach($information_group_id,array('information_group_id'=>$information_group_id));
        }*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateArticleForm() === true) {
            $Article = $this->saveArticleProperties($request);

            if (!is_null($Article)) {
                $this->saveArticleDetail($request, $Article);
                $this->saveArticletToArticle($request, $Article);
            }

            // redirect
            Session::flash('message', 'Successfully created article!');

            return Redirect::to('admin/articles');
        } else {
            return $this->validateArticleForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateArticleForm() === true) {
            $Article = $this->saveArticleProperties($request, $id);

            if (!is_null($Article)) {
                $this->saveArticleDetail($request, $Article);
                $this->saveArticletToArticle($request, $Article);
            }

            // redirect
            Session::flash('message', 'Successfully updated article!');

            return Redirect::to('admin/articles');
        } else {
            return $this->validateArticleForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Detail = Detail::find($id);
        ArticleToArticle::where('article_id', '=', $id)->orWhere('article_tag_id', '=', $id)->delete();

        $mainArticleID = $Detail->article_id;

        //find the sortid of this product... so all the above can descent by 1
        $updateInformations = Detail::where('language_id', '=', $Detail->language_id)->get(['id', 'sort_id']);
        if (isset($updateInformations) && count($updateInformations) > 0) {
            foreach ($updateInformations as $uInformation) {
                $uInformation->sort_id = intval($uInformation->sort_id) - 1;
                $uInformation->save();
            }//end foreach($updateInformations as $Information)
        }//end 	if (count($updateInformations)>0)

        $Detail->delete();

        if (Detail::where("article_id", "=", $mainArticleID)->count() <= 0) {
            Article::destroy($mainArticleID);
        }

        Session::flash('message', 'Successfully deleted the article!');

        return Redirect::to('admin/articles');
    }

    public function saveArticletToArticle(Request $request, $Article = null)
    {
        ArticleToArticle::where('article_tag_id', '=', $Article->id)->delete();
        $Article->article()->sync($request->get("article_id"));
    }

    public function getArticleRelation($article_id = null)
    {
        $query = DB::connection('project')
            ->table('articles')
            ->select(
                (
                DB::connection("project")->raw('
												articles.id,
                                                articles_language.title as `title`,
                                                articles_categories_language.title as `category`,
												CASE WHEN (SELECT count(*) FROM articles_to_articles 
															WHERE 
															(articles_to_articles.article_id = articles.id and article_tag_id = "' . $article_id . '") 
															OR 
															(articles_to_articles.article_tag_id = articles.id and article_id = "' . $article_id . '")
														) > 0 THEN 1 ELSE 0 END AS checked
											')
                )
            )->leftjoin('articles_language', 'articles.id', '=', 'articles_language.article_id')
            ->leftJoin('articles_categories_language', 'articles_categories_language.id', '=', 'articles_language.language_id')
            ->where('articles_language.language_id', '=', '1')
            ->where('article_id', '<>', $article_id)
            ->orderBy("articles_language.title");

        return DataTables::queryBuilder($query)
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="article_id[]" value="' . $model->id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->id . '" > ';
            })
            ->rawColumns(['radio'])
            ->make(true);
    }
}
