/*
Navicat MySQL Data Transfer

Source Server         : Combell_newserver
Source Server Version : 50623
Source Host           : 178.208.48.50:3306
Source Database       : dcms_groupdc_be

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2016-06-20 09:09:47
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newarticle` tinyint(4) DEFAULT '0',
  `enabletime` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `birthdaycycle` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `highpriority` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `enablemorebtn` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `online` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `actionarticle` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `calendar` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `archive` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `thumbnailindetail` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `deleted` tinyint(4) DEFAULT '0',
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT null,
  `updated_at` timestamp NULL DEFAULT null,
  PRIMARY KEY (`id`),
  KEY `i_Highpriority` (`highpriority`),
  KEY `i_Enabletime` (`enabletime`),
  KEY `i_startdate` (`startdate`),
  KEY `i_enddate` (`enddate`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------

-- ----------------------------
-- Table structure for `articles_categories_language`
-- ----------------------------
DROP TABLE IF EXISTS `articles_categories_language`;
CREATE TABLE `articles_categories_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT null,
  `updated_at` timestamp NULL DEFAULT null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles_categories_language
-- ----------------------------
INSERT INTO `articles_categories_language` VALUES ('1', null, '1', '2', '0', '1', 'news', 'news', 'news', null, '2016-06-17 08:56:04', '2016-06-17 08:56:04');
INSERT INTO `articles_categories_language` VALUES ('2', null, '3', '4', '0', '2', 'news', 'news', 'news', null, '2016-06-17 08:57:29', '2016-06-17 08:57:29');

-- ----------------------------
-- Table structure for `articles_language`
-- ----------------------------
DROP TABLE IF EXISTS `articles_language`;
CREATE TABLE `articles_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) unsigned DEFAULT '1',
  `article_category_id` int(11) unsigned DEFAULT NULL,
  `article_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'custom dcm - extra url to image or external doc/link',
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT null,
  `updated_at` timestamp NULL DEFAULT null,
  PRIMARY KEY (`id`),
  KEY `FK_articlelanguage` (`language_id`),
  KEY `FK_categoryid` (`article_category_id`),
  KEY `FK_articleid` (`article_id`),
  FULLTEXT KEY `ArticleSearchHelper` (`title`,`description`),
  CONSTRAINT `articles_language_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `articles_language_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `articles_language_ibfk_3` FOREIGN KEY (`article_category_id`) REFERENCES `articles_categories_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles_language
-- ----------------------------

-- ----------------------------
-- Table structure for `articles_language_to_pages`
-- ----------------------------
DROP TABLE IF EXISTS `articles_language_to_pages`;
CREATE TABLE `articles_language_to_pages` (
  `article_detail_id` int(11) unsigned DEFAULT NULL,
  `page_id` int(11) unsigned DEFAULT NULL,
  KEY `FK_articledetail_id` (`article_detail_id`),
  KEY `FK_pageid` (`page_id`),
  CONSTRAINT `articles_language_to_pages_ibfk_1` FOREIGN KEY (`article_detail_id`) REFERENCES `articles_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `articles_language_to_pages_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `pages_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articles_language_to_pages
-- ----------------------------


-- ----------------------------
-- Table structure for `articles_to_articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles_to_articles`;
CREATE TABLE `articles_to_articles` (
  `article_id` int(11) unsigned NOT NULL DEFAULT '0',
  `article_tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  KEY `FK_article_id2` (`article_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `articles_to_articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles_to_articles`;
CREATE TABLE `articles_to_articles` (
  `article_id` int(11) unsigned NOT NULL DEFAULT '0',
  `article_tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  KEY `FK_article_id2` (`article_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articles_to_articles
-- ----------------------------

-- ----------------------------
-- View structure for `vwarticles_to_articles`
-- ----------------------------
DROP VIEW IF EXISTS `vwarticles_to_articles`;
CREATE ALGORITHM=UNDEFINED DEFINER=`marketing`@`94.107.236.219` SQL SECURITY DEFINER VIEW `vwarticles_to_articles` AS select `articles_to_articles`.`article_id` AS `article_id`,`articles_to_articles`.`article_tag_id` AS `article_tag_id` from `articles_to_articles` union select `articles_to_articles`.`article_tag_id` AS `article_tag_id`,`articles_to_articles`.`article_id` AS `article_id` from `articles_to_articles`;