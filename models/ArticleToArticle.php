<?php

namespace Dcms\Articles\Models;

use Dcms\Core\Models\EloquentDefaults;

class ArticleToArticle extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "articles_to_articles";
}
