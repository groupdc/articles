<?php

namespace Dcms\Articles\Models;

use Dcms\Core\Models\EloquentDefaults;

class Article extends EloquentDefaults
{
    protected $connection = 'project';

    public function detail()
    {
        return $this->hasMany('Dcms\Articles\Models\Detail', 'article_id', 'id');
    }

    public function article()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Articles\Models\Article', 'articles_to_articles', 'article_id', 'article_tag_id');
    }

    public function category()
    {
        return $this->belongsTo("Dcms\Articles\Models\Category", 'article_category_id', 'id');
        //return $this->hasOne("Category");
    }

    public function productinformation()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        //	return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Information', 'articles_to_products_information_group',  'article_id','information_id');
    }
}
