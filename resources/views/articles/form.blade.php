@extends("dcms::template/layout")

@section("content")


    <div class="main-header">
      <h1>Articles</h1>
      <ol class="breadcrumb">
        <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
        <li><a href="{!! URL::to('admin/articles') !!}"><i class="fa fa-pencil"></i> Articles</a></li>
@if(isset($article))
        <li class="active">Edit</li>
@else
        <li class="active">Create</li>
@endif
      </ol>
    </div>

    <div class="main-content">
      @if(isset($article))
          {!! Form::model($article, array('route' => array('admin.articles.update', $article->id), 'method' => 'PUT')) !!}
      @else
          {!! Form::open(array('url' => 'admin/articles')) !!}
      @endif
      <div class="row">
        <div class="col-md-9">
    	    <div class="main-content-tab tab-container">
		      @if (!is_array($categoryOptionValues) || count($categoryOptionValues)<=0 ) 	Please first create a <a href="{!! URL::to('admin/articles/categories/create') !!}"> article category </a>  @else
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#information" role="tab" data-toggle="tab">Information</a></li>
              <li><a href="#pages" role="tab" data-toggle="tab">Pages</a></li>
              <li><a href="#articles" role="tab" data-toggle="tab">Articles</a></li>
              @yield('extratabs')
            </ul>

            <div class="tab-content">
              <div id="information" class="tab-pane active">
                <!-- #information -->
                @if($errors->any())
                  <div class="alert alert-danger">{!! Html::ul($errors->all()) !!}</div>
                @endif

                @if(isset($languages))
                  <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $key => $language)
                    <li class="{!! ($key == 0 ? 'active' : '') !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab" data-toggle="tab"><img src="{!! asset('/packages/dcms/core/images/flag-' . strtolower($language->country) . '.png') !!}" width="16" height="16" /> {!! $language->language_name !!}</a></li>
                    @endforeach
                  </ul>

                  <div class="tab-content">
                    @foreach($languages as $key => $information)
                    <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! ($key == 0 ? 'active' : '') !!}">
                      {!! Form::hidden('article_information_id[' . $information->language_id . ']', $information->id) !!}

                      <div class="form-group">
                        {!! Form::label('category_id[' . $information->language_id . ']', 'Category') !!}
                        {!! isset($categoryOptionValues[$information->language_id])? Form::select('category_id[' . $information->language_id . ']', $categoryOptionValues[$information->language_id], (old('category_id[' . $information->language_id . ']') ? old('category_id[' . $information->language_id . ']') : $information->article_category_id), array('class' => 'form-control')):'No categories found' !!}
                      </div>

                      <div class="form-group">
                        {!! Form::label('title[' . $information->language_id . ']', 'Title') !!}
                        {!! Form::text('title[' . $information->language_id . ']', (old('title[' . $information->language_id . ']') ? old('title[' . $information->language_id . ']') : $information->title ), array('class' => 'form-control')) !!}
                      </div>

                      <div class="form-group">
                        {!! Form::label('description[' . $information->language_id . ']', 'Description') !!}
                        {!! Form::textarea('description[' . $information->language_id . ']', (old('description[' . $information->language_id . ']') ? old('description[' . $information->language_id . ']') : $information->description ), array('class' => 'form-control ckeditor')) !!}
                      </div>

                      <div class="form-group">
                        {!! Form::label('body[' . $information->language_id . ']', 'Body') !!}
                        {!! Form::textarea('body[' . $information->language_id . ']', (old('body[' . $information->language_id . ']') ? old('body[' . $information->language_id . ']') : $information->body ), array('class' => 'form-control ckeditor')) !!}
                      </div>

                        <div class="form-group">
                            {!! Form::label('url', 'url') !!}
                            <div class="input-group">
                                {!! Form::text('url[' . $information->language_id . ']', (old('url[' . $information->language_id . ']') ? old('url[' . $information->language_id . ']') : $information->url ), array('class' => 'form-control', 'id'=>'url'.$information->language_id )) !!}
                                <span class="input-group-btn">
                                {!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server', 'id'=>'browse_url'.$information->language_id )) !!}
                                </span>
                            </div>
                        </div>

                    </div>
                    @endforeach
                  </div>

                @endif
                <!-- #information -->
              </div>
              <div id="pages" class="tab-pane">
                <div class="tab-content">

                  <?php
                    // *Very simple* recursive rendering function
                    function renderNode($node, $pageOptionValuesSelected = array())
                    {
                        echo '<li class="language-'.$node->language_id.' depth-'.$node->depth.'  ">';
                        echo '<span>';
                        if ($node->depth == 0) {
                            echo $node->title .'<i class="fa fa-plus-square"></i>';
                        } else {
                            $checked = false;
                            if (in_array($node->id, $pageOptionValuesSelected)) {
                                $checked = true;
                            } ?>
                          {!! Form::checkbox("page_id[".$node->language_id."][".$node->id."]", $node->id, $checked, array('class' => 'form-checkbox','id'=>'page_id-'.$node->id))  !!}
                          {!! Form::label('page_id-'.$node->id, $node->title, array('class' => ($checked == true?'active':'').' checkbox','id'=>'chkbxpage_id-'.$node->id)) !!}
                        <?php
                        }
                        echo '</span>';

                        if ($node->children()->count() > 0) {
                            echo "\r\n".'<ul class="'.($node->depth==0?'division':($node->depth==1?'sector':'subsector')).'">'."\r\n";
                            foreach ($node->children as $child) {
                                renderNode($child, $pageOptionValuesSelected);
                            }
                            echo '</ul>'."\r\n"."\r\n";
                        }
                        echo '</li>'."\r\n";
                    }

                    $roots = Dcms\Pages\Models\Pages::withDepth()->having('depth', '=', 0)->get();

                    echo '<ul class="country">';
                    foreach ($roots as $root) {
                        renderNode($root, $pageOptionValuesSelected);
                    }
                    echo '</ul>';
                    ?>

                </div>
              </div>


              <div id="articles" class="tab-pane">
                <div class="tab-content">

									<div class="form-group">
											<table id="datatablearticles" class="table table-hover table-condensed" style="width:100%">
													<thead>
															<tr>
															<th style="width:40px;"></th>
                              <th style="width:200px;">Category</th>
															<th>Title</th>
															</tr>
													</thead>
											</table>

											<script type="text/javascript">
													$(document).ready(function() {
															oTable = $('#datatablearticles').DataTable({
																"pageLength": 9999,
																"processing": true,
																"serverSide": false,
																@if(isset($article))
																		"ajax": "{{ route('admin.articles.articlerelation',array($article->id)) }}",
																@else
																		"ajax": "{{ route('admin.articles.articlerelation') }}",
																@endif
																"columns": [
																				{data: 'radio', name: 'radio', orderable: false, searchable: false},
                                        {data: 'category', name: 'articles_categories_language.title'},
																				{data: 'title', name: 'title'}
																		]
															});
													});
											</script>

											<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

											<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
											<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

									</div>
                </div>
              </div>

            </div>
				  </div>
        </div>
        <div class="col-md-3">
          <!-- -->
					<div class="main-content-block">
            <div class="form-group setdate">
              {!! Form::checkbox('enabletime', '1', null, array('class' => 'form-checkbox','id'=>'enabletime'))  !!}
              {!! Html::decode(Form::label('enabletime', 'Set Date', array('class' => (isset($article) && $article->enabletime==1)?'checkbox active':'checkbox'))) !!}
            </div>
            <!-- -->
            <div class="form-group startdate">
              {!! Form::label('startdate', 'Date Start') !!}
              <div class="input-group input-append date">
                {!! Form::text('startdate', old('startdate'), array('id'=>'Startdate','class' => 'form-control', 'readonly', 'size' => '16')) !!}
                <span class="input-group-addon btn btn-primary"><i class="glyphicon glyphicon-th"></i></span>
              </div>
            </div>
            <!-- -->
            <div class="form-group enddate">
              {!! Form::label('enddate', 'Date End') !!}
              <div class="input-group input-append date">
                {!! Form::text('enddate', old('enddate'), array('id'=>'Enddate','class' => 'form-control', 'readonly', 'size' => '16')) !!}
                <span class="input-group-addon btn btn-primary"><i class="glyphicon glyphicon-th"></i></span>
              </div>
            </div>
            <!-- -->
            <div class="form-group archive">
              {!! Form::checkbox('archive', '1', null, array('class' => 'form-checkbox','id'=>'archive'))  !!}
              {!! Html::decode(Form::label('archive', 'Archive', array('class' => (isset($article) && $article->archive==1)?'checkbox active':'checkbox'))) !!}
            </div>
            <!-- -->
            <div class="form-group">
              {!! Form::checkbox('birthdaycycle', '1', null, array('class' => 'form-checkbox','id'=>'birthdaycycle'))  !!}
              {!! Html::decode(Form::label('birthdaycycle', 'Repeating', array('class' => (isset($article) && $article->birthdaycycle==1)?'checkbox active':'checkbox'))) !!}
            </div>
            <!-- -->
            <div class="form-group">
              {!! Form::checkbox('highpriority', '1', null, array('class' => 'form-checkbox','id'=>'highpriority'))  !!}
              {!! Html::decode(Form::label('highpriority', 'High Priority', array('class' => (isset($article) && $article->highpriority==1)?'checkbox active':'checkbox'))) !!}
            </div>
            <!-- -->
            <div class="form-group">
              {!! Form::checkbox('online', '1', null, array('class' => 'form-checkbox','id'=>'online'))  !!}
              {!! HTML::decode(Form::label('online', 'Online', array('class' => (isset($article) && $article->online==1)?'checkbox active':'checkbox'))) !!}
            </div>
            <!-- -->
            <div class="form-group">
              {!! Form::checkbox('newarticle',1, null, array('class' => 'form-checkbox','id'=>'newarticle'))!!}
              {!! Html::decode(Form::label('newarticle', "New Article", array('class' => (isset($article) && $article->newarticle==1)?'checkbox active':'checkbox'))) !!}
            </div>

            <div class="form-group">
              {!! Form::checkbox('calendar',1, null, array('class' => 'form-checkbox','id'=>'calendar'))!!}
              {!! Html::decode(Form::label('calendar', "Calendar", array('class' => (isset($article) && $article->calendar==1)?'checkbox active':'checkbox'))) !!}
            </div>

          {{--  @if(!isset($articleFormTemplate)  || is_null($articleFormTemplate) )

            @elseif(!is_null($articleFormTemplate))
              @include($articleFormTemplate)
              @yield('mainForm')
            @endif --}}

					</div>
          <!-- -->
          <div class="main-content-block">
						 <div class="form-group">
              {!! Form::label('thumbnail', 'Thumbnail') !!}
              <div class="thumbnail">
              <img src="{{(isset($article) ?  $article->thumbnail: '') }}">
              </div>
              <div class="input-group">
                  {!! Form::text('thumbnail', old('thumbnail'), array('class' => 'form-control')) !!}
                <span class="input-group-btn">
                  {!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server', 'id'=>'browse_thumbnail')) !!}
                </span>
              </div>
            </div>
					</div>
          <!-- -->
              <?php
              /*
          <div class="main-content-block">

                <div class="form-group">
                  {!! Form::label('category_id[' . $information->language_id . ']', 'Block Template') !!}
                  {!! isset($categoryOptionValues[$information->language_id])? Form::select('category_id[' . $information->language_id . ']', $categoryOptionValues[$information->language_id], (old('category_id[' . $information->language_id . ']') ? old('category_id[' . $information->language_id . ']') : $information->article_category_id), array('class' => 'form-control')):'No categories found' !!}
                </div>

                <!--
                  <div class="form-group">
                    {!! Form::label('category_id[' . $information->language_id . ']', 'Block Position') !!}
                    {!! isset($categoryOptionValues[$information->language_id])? Form::select('category_id[' . $information->language_id . ']', $categoryOptionValues[$information->language_id], (old('category_id[' . $information->language_id . ']') ? old('category_id[' . $information->language_id . ']') : $information->article_category_id), array('class' => 'form-control')):'No categories found' !!}
                  </div>
                -->
                </div>*/
        ?>
          <!-- -->
        </div>
		@endif
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="main-content-block">
            {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
            <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>

@stop

@section("script")

<script type="text/javascript" src="{!! asset('/packages/dcms/core/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/dcms/core/js/bootstrap-datetimepicker.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('/packages/dcms/core/css/bootstrap-datetimepicker.min.css') !!}">

<script type="text/javascript" src="{!! asset('/packages/dcms/core/ckeditor/ckeditor.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/dcms/core/ckeditor/adapters/jquery.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/dcms/core/ckfinder/ckfinder.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/dcms/core/ckfinder/ckbrowser.js') !!}"></script>

<script type="text/javascript">
$(document).ready(function() {


  	//CKFinder for CKEditor
  	CKFinder.setupCKEditor( null, '/packages/dcms/core/ckfinder/' );

  	//CKFinder
  	$(".browse-server").click(function() {
  		BrowseServer( 'Images:/articles/', 'thumbnail' );
  	})

    $("body").on("click",".browse-server-files", function(){
      var returnid = $(this).attr("id").replace("browse_","") ;
      BrowseServer( 'Files:/', returnid);
		});


  	//CKEditor
  	$("textarea[id='description']").ckeditor();
  	$("textarea[id='body']").ckeditor();


	//Bootstrap Tabs
	$(".tab-container .nav-tabs a").click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})


	//Datepicker
	ToggleDate();

	$(".setdate label").click(function() {
	ToggleDate();
	});

	//pagetree
	$(".country span").click(function() {
	$(this).find('i').toggleClass('fa-minus-square');
	$(this).next().toggleClass('active');
	});


});

function ToggleDate() {
  if($('.setdate label').hasClass('active')) {
    $('.startdate, .enddate').addClass('active');
  }else{
    $('.startdate, .enddate').removeClass('active');
  }
}
$(function() {
    $('#Startdate, #Enddate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    });
});

</script>

@stop
